# OpenML dataset: CookbookReviews

https://www.openml.org/d/45744

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The "Cookbook Reviews" is an extensive data set that includes a range of information about user interactions and recipe reviews. It contains important details like the recipe name, where it stands in the list of the top 100 recipes, a special recipe code, and user information like the user ID, name, and internal reputation rate. A unique comment ID and other attributes, such as the creation timestamp, response count, and the total number of upvotes and downvotes, are attached to each review comment. Users' opinions of recipes are rated on a 5-star rating system, with 0 representing no rating at all.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45744) of an [OpenML dataset](https://www.openml.org/d/45744). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45744/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45744/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45744/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

